package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        List result = new ArrayList();
        if (x != null && y != null) {
            List list_y = new LinkedList(y);
            for (Object objX : x) {
                Iterator iterator = list_y.iterator();
                while (iterator.hasNext()) {
                    if (!iterator.next().equals(objX)) {
                        iterator.remove();
                    } else {
                        result.add(objX);
                        break;
                    }
                }
            }
        } else throw new IllegalArgumentException();
        if (x.equals(result))
            return true;
        return false;
    }
}
