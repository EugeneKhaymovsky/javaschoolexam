package com.tsystems.javaschool.tasks.pyramid;

        import java.util.Arrays;
        import java.util.Collections;
        import java.util.List;


public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        if (inputNumbers.size() > Integer.MAX_VALUE - 8 || inputNumbers.contains(null)) throw new CannotBuildPyramidException();
        int[][] result;
        int count = 0;
        int row = 1;
        int column = 1;
        boolean possible;
        while (count < inputNumbers.size()) {
            count = count + row;
            row++;
            column = column + 2;
        }
        row = row - 1;
        column = column - 2;

        if (inputNumbers.size() == count) {
            possible = true;
        } else possible = false;
        if (possible) {
            Collections.sort(inputNumbers);
            result = new int[row][column];
            for (int[] i : result) {
                Arrays.fill(i, 0);
            }
            int centerPoint = (column / 2);
            count = 1;
            int arrayIndex = 0;
            for (int i = 0, move = 0; i < row; i++, move++, count++) {
                int start = centerPoint - move;
                for (int j = 0; j < count * 2; j += 2, arrayIndex++) {
                    result[i][start + j] = inputNumbers.get(arrayIndex);
                }
            }
        } else throw new CannotBuildPyramidException();

        return result;
    }
}
