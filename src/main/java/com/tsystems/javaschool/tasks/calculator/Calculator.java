package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.StringTokenizer;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        String result;
        try {
            String test = notation(statement);
            double res = calculation(test);
            if (res == (int)res) result = (int)res + "";
            else result = res + "";
        } catch (Exception e) {
            result = null;
        }
        return result;
    }

    private static String notation(String lineIn) throws Exception {
        StringBuilder lineStack = new StringBuilder();
        StringBuilder lineResult = new StringBuilder();
        char charIn;
        char charTemp;

        for (int i = 0; i < lineIn.length(); i++) {
            charIn = lineIn.charAt(i);
            if (isOperator(charIn)) {
                while (lineStack.length() > 0) {
                    charTemp = lineStack.substring(lineStack.length()-1).charAt(0);
                    if (isOperator(charTemp) && (operationPriority(charIn) <= operationPriority(charTemp))) {
                        lineResult.append(" ").append(charTemp).append(" ");
                        lineStack.setLength(lineStack.length()-1);
                    } else {
                        lineResult.append(" ");
                        break;
                    }
                }
                lineResult.append(" ");
                lineStack.append(charIn);
            } else if (charIn == '(') {
                lineStack.append(charIn);
            } else if (charIn == ')') {
                charTemp = lineStack.substring(lineStack.length()-1).charAt(0);
                while (charTemp != '(') {
                    if (lineStack.length() < 1) throw new Exception();
                    lineResult.append(" ").append(charTemp);
                    lineStack.setLength(lineStack.length()-1);
                    charTemp = lineStack.substring(lineStack.length()-1).charAt(0);
                }
                lineStack.setLength(lineStack.length()-1);
            } else lineResult.append(charIn);
        }

        while (lineStack.length() > 0) {
            lineResult.append(" ").append(lineStack.substring(lineStack.length()-1));
            lineStack.setLength(lineStack.length()-1);
        }
        return lineResult.toString();
    }

    private static boolean isOperator(char c) {
        switch (c) {
            case '+': return true;
            case '-': return true;
            case '*': return true;
            case '/': return true;
        }
        return false;
    }

    private static int operationPriority(char operator) {
        int priority = 0;
        switch (operator) {
            case '*' : priority = 2;
                break;
            case '/' : priority = 2;
                break;
            case '+' : priority = 1;
                break;
            case '-' : priority = 1;
        }
        return priority;
    }

    private static double calculation(String line) throws Exception {
        double result;
        double num1;
        double num2;
        String tempLine;
        Deque<Double> stack = new ArrayDeque<Double>();
        StringTokenizer tokenizer = new StringTokenizer(line);
        while(tokenizer.hasMoreTokens()) {
            try {
                tempLine = tokenizer.nextToken().trim();
                if (tempLine.length() == 1 && isOperator(tempLine.charAt(0))) {
                    if (stack.size() < 2) throw new Exception();
                    num2 = stack.pop();
                    num1 = stack.pop();
                    switch (tempLine.charAt(0)) {
                        case '+':
                            num1 += num2;
                            break;
                        case '-':
                            num1 -= num2;
                            break;
                        case '/':
                            num1 /= num2;
                            break;
                        case '*':
                            num1 *= num2;
                            break;
                        default:
                            throw new Exception();
                    }
                    stack.push(num1);
                } else {
                    num1 = Double.parseDouble(tempLine);
                    stack.push(num1);
                }
            } catch (Exception e) {
                throw new Exception();
            }
        }
        if (stack.size() > 1) {
            throw new Exception();
        }
        result = stack.pop();
        if(result == Double.POSITIVE_INFINITY)
            throw new Exception();

        return result;
    }
}
